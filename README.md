n3t Template
============

n3tTemplate component is content templating extension for Joomla. Some of it features are:

  * Define templates (code snippets) in the backend, categorize it,
    publish it, asssign access level to it etc..
  * Access the tree of such defined templates by button displayed below 
    the editor of content article, and insert it to your editor. 
  * Define which template should automatically load to editor for
    which content category. 
  * Specify template which will be automatically prepended or appended
    to content article in specific category. 
  * Use powerfull plugin system, to select other content than templates
    (files, folders, or even youtube videos) 
    and insert links to it or 3rd party plugin codes to display it. 
  * Use indirect templating to display same code in many articles, and 
    still have chance to modify it on one place.
    
Documentation
-------------

Find more at [documentation page](http://n3ttemplate.docs.n3t.cz/en/latest/)    