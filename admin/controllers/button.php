<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class n3tTemplateControllerButton extends JControllerLegacy {

	function __construct($config=array()) {
		parent::__construct($config);
    $access=n3tTemplateHelperButton::getButtonAccess();
    $user = JFactory::getUser();
    if (!in_array($access, $user->getAuthorisedViewLevels())) {
      JError::raiseError( 403, JText::_('COM_N3TTEMPLATE_NOT_AUTHORIZED') );  
    }		
	}
	
	function display($cachable = false, $urlparams = array()) {
  	parent::display();
	}
	
	function preview() {
  	$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_N3TTEMPLATE_TEMPLATE_PREVIEW'));
		$document->setBase(JURI::root());

	  $model = $this->getModel('button');
	  echo $model->getTemplate();
	}

	function template() {
  	$app = JFactory::getApplication();

	  $model = $this->getModel('button');
	  echo $model->getTemplate();
	
	  JResponse::setHeader('Content-Type', 'text/html; charset=utf-8');
    JResponse::sendHeaders();    
	  $app->close();
	}

	function autotemplate() {
  	$app = JFactory::getApplication();

	  $model = $this->getModel('button');
	  echo $model->getAutoTemplate();
	  
	  JResponse::setHeader('Content-Type', 'text/html; charset=utf-8');
    JResponse::sendHeaders();	  
	  $app->close();
	}
	
}
