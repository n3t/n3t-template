<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('JPATH_BASE') or die();

jimport('joomla.form.formfield');

class JFormFieldWarning extends JFormField
{
	protected $type = 'Warning';

	protected function getInput()
	{		
		$table = $this->element['table'];		
		$warning = $this->element['warning'];
    
    if ($table && $warning) {
      $db = JFactory::getDBO();
		  $tables = $db->getTableList();
		  $table = str_replace( '#__', $db->getPrefix(), $table);
		  if (array_search($table, $tables) === false)
		    return '<p class="alert alert-error">'.JText::_($warning).'</p>';    
    }
      
		return '';
	}
}