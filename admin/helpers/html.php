<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

jimport('joomla.html.pane');
jimport('joomla.application.module.helper'); 

class n3tTemplateHelperHTML extends JObject
{
  
  public static function toolbarTitle($text = false) 
  {
    JToolBarHelper::title(JText::_('COM_N3TTEMPLATE') . ': ' . $text, 'n3ttemplate');
  }
    
  public static function subtoolbar($vName = '') 
  {
    $user = JFactory::getUser();
    if ($user->authorise('core.manage', 'com_n3ttemplate')) {
      JHtmlSidebar::addEntry(JText::_('COM_N3TTEMPLATE_CATEGORIES'), 'index.php?option=com_n3ttemplate&view=categories', $vName == 'categories');    
      JHtmlSidebar::addEntry(JText::_('COM_N3TTEMPLATE_TEMPLATES'), 'index.php?option=com_n3ttemplate&view=templates', $vName == 'templates');
      JHtmlSidebar::addEntry(JText::_('COM_N3TTEMPLATE_AUTOTEMPLATES'), 'index.php?option=com_n3ttemplate&view=autotemplates', $vName == 'autotemplates');
    }    
    if ($user->authorise('core.admin', 'com_n3ttemplate')) {
      JHtmlSidebar::addEntry(JText::_('COM_N3TTEMPLATE_PLUGINS'), 'index.php?option=com_plugins&view=plugins&filter_folder=n3ttemplate');
    }
    
    if ($user->authorise('core.manage', 'com_postinstall')) {
      $component = JComponentHelper::getComponent('com_n3ttemplate');
  		JHtmlSidebar::addEntry(
  			JText::_('COM_N3TTEMPLATE_SIDEBAR_POSTINSTALL_MESSAGES'),
  			'index.php?option=com_postinstall&eid='.$component->id			
  		);                    
    }
  }  

  public static function assetsPopup() 
  {
		$doc= JFactory::getDocument();
	  $doc->addStyleSheet(JURI::root(true).'/media/com_n3ttemplate/n3ttemplate.popup.css');
  }

	public static function categoryTreeOptions( $excludeid = null, $alltext = false )
	{
		$db = JFactory::getDBO();

		if ( $excludeid ) {
			$excludeid = ' AND id != '.(int) $excludeid;
		} else {
			$excludeid = null;
		}

		$query = 'SELECT *, title AS name, parent_id AS parent' .
				' FROM #__n3ttemplate_categories c' .
				' WHERE published != -2 AND plugin = ""' .
				$excludeid .
				' ORDER BY parent_id, ordering';
		$db->setQuery( $query );
		$items = $db->loadObjectList();

		$children = array();

		if ( $items )
		{
			foreach ( $items as $v )
			{			  
				$pt 	= $v->parent_id;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}

		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );

		$items 	= array();
		if ($alltext) $items[] = JHTML::_('select.option',  '-1', $alltext );
		$items[] = JHTML::_('select.option',  '0', JText::_( 'COM_N3TTEMPLATE_UNCATEGORIZED' ) );

		foreach ( $list as $item ) {
  	  $items[] = JHTML::_('select.option',  $item->id, $item->treename );
		}
    
		return $items;
	}

	public static function categoryTree( $id, $fieldname, $excludeid = null, $size = 10, $autosubmit = false, $alltext = false )
	{
		if (!$id) $id = 0;
    if ($autosubmit) $autosubmit = ' onchange="this.form.submit();"';
		$output = JHTML::_('select.genericlist',   static::categoryTreeOptions($excludeid, $alltext), $fieldname, 'class="input-large" size="'.$size.'"'.$autosubmit, 'value', 'text', $id );

		return $output;
	}
	
	public static function booleanList( $name, $value = null, $id = null ) {
		$arr = array(
			JHTML::_('select.option', '0', JText::_('JNO')),
			JHTML::_('select.option', '1', JText::_('JYES'))
		);
		return JHTML::_('select.genericlist',  $arr, $name, 'class="input-medium" size="1"', 'value', 'text', (int)$value, $id );
	}

  public static function filterState($filter_state) {
    $states = array();
    $states[]=JHTML::_('select.option',2,JText::_('COM_N3TTEMPLATE_FILTER_CURRENT'),'id','title');
    $states[]=JHTML::_('select.option',1,JText::_('COM_N3TTEMPLATE_FILTER_PUBLISHED'),'id','title');
    $states[]=JHTML::_('select.option',0,JText::_('COM_N3TTEMPLATE_FILTER_UNPUBLISHED'),'id','title');
    $states[]=JHTML::_('select.option',-2,JText::_('COM_N3TTEMPLATE_FILTER_TRASHED'),'id','title');
    return JHTML::_('select.genericlist',$states,'filter_state','class="input-large" size="1" onchange="this.form.submit();"','id','title',$filter_state);    
  }

  public static function filterPositionOptions($empty = true) {
    $positions = array();
    if ($empty)
      $positions[]=JHTML::_('select.option','',JText::_('COM_N3TTEMPLATE_SELECT_ARTICLE_POSITION'),'id','title');
    $positions[]=JHTML::_('select.option','editor',JText::_('COM_N3TTEMPLATE_ARTICLE_POSITION_EDITOR'),'id','title');
    $positions[]=JHTML::_('select.option','top',JText::_('COM_N3TTEMPLATE_ARTICLE_POSITION_TOP'),'id','title');
    $positions[]=JHTML::_('select.option','bottom',JText::_('COM_N3TTEMPLATE_ARTICLE_POSITION_BOTTOM'),'id','title');
    $positions[]=JHTML::_('select.option','introtop',JText::_('COM_N3TTEMPLATE_ARTICLE_POSITION_INTROTOP'),'id','title');
    $positions[]=JHTML::_('select.option','introbottom',JText::_('COM_N3TTEMPLATE_ARTICLE_POSITION_INTROBOTTOM'),'id','title');
    return $positions; 
  }

  public static function filterPosition($filter_position) {
    return JHTML::_('select.genericlist',static::filterPositionOptions(),'position','class="input-large" size="1" onchange="this.form.submit();"','id','title',$filter_position);    
  }
  
  public static function displayAccess($access, $groupname) {
		return '<span>'. JText::_( $groupname ) .'</span>';
  }
  
  public static function ordering($value, $id, $query, $neworder = 0) {
		if (is_object($value)) $value = $value->ordering;
		if ($id) $neworder = 0;
		else $neworder = $neworder ? -1 : 1;
		return JHTML::_('list.ordering','ordering', $query, '', $value, $neworder);      
  }

	public static function accesslevel($value, $name = 'access', $emptyvalue = false)
	{
    if ($emptyvalue)
	    return JHtml::_('access.assetgrouplist', $name, $value, array('class' => 'input-large', 'size' => '1'), array('title' => $emptyvalue ));
	  else
	    return JHtml::_('access.assetgrouplist', $name, $value, array('class' => 'input-large', 'size' => '1'));
	}
  
	public static function publishedIcon($value, $img1 = 'tick.png', $img0 = 'publish_x.png')
	{
		$img	= $value ? $img1 : $img0;		
    
    $alt	= $value ? JText::_('JPUBLISHED') : JText::_('JUNPUBLISHED');
	  return $href = JHtml::_('image','admin/'.$img, $alt, NULL, true);
	}
  
	public static function pluginsList( $name, $value = null, $id = null ) {
	  $plugins = n3tTemplateHelperPlugin::loadPlugins();
	  $empty = new StdClass();
	  $empty->plugin = '';
	  $empty->name = '';
    $plugins = array_merge(array($empty), $plugins); 
		return JHTML::_('select.genericlist',  $plugins, $name, 'class="input-large" size="1" onchange="checkPluginParams();"', 'plugin', 'name', $value, $id );
	}        
	
  public static function contentCategories($name, $selectedCategories) {
    $db = JFactory::getDBO();
    $categories = JHtml::_('category.options', 'com_content');
		$category = JHTML::_('select.genericlist', $categories, $name.'[]', 'class="input-large" size="10" multiple="multiple"', 'value', 'text', $selectedCategories );
		return $category;     
  }
  
  public static function batchMoveCopy()
  {
		$options = array(
			JHtml::_('select.option', 'copy', JText::_('COM_N3TTEMPLATE_BATCH_COPY')),
			JHtml::_('select.option', 'move', JText::_('COM_N3TTEMPLATE_BATCH_MOVE'))
		);    
		return JHTML::_( 'select.radiolist', $options, 'batch[movecopy]', '', 'value', 'text', 'move');
  }

	public static function getArticlePositions() {
    return array(
      'editor',
      'top',
      'bottom',
      'introtop',
      'introbottom'
    ); 
	}
    
	public static function postInstallMessages() {    
    $user = JFactory::getUser();
    if ($user->authorise('core.manage', 'com_postinstall')) {
      jimport( 'fof.model.model' );		
      
      $component = JComponentHelper::getComponent('com_n3ttemplate');
  		$messages_model = FOFModel::getTmpInstance('Messages', 'PostinstallModel')->eid($component->id);
  		$messages = $messages_model->getItemList();
      if (count($messages)) {
        $html = array();		
				$html[] = '<div class="alert alert-info">';
				$html[] = '<h4>'.JText::_('COM_N3TTEMPLATE_POSTINSTALL_MESSAGES_TITLE').'</h4>';
				$html[] = '<p>'.JText::_('COM_N3TTEMPLATE_POSTINSTALL_MESSAGES_BODY_NOCLOSE').'</p>';
				$html[] = '<p>'.JText::_('COM_N3TTEMPLATE_POSTINSTALL_MESSAGES_BODYMORE_NOCLOSE').'</p>';
				$html[] = '<p><a href="index.php?option=com_postinstall&eid='.$component->id.'" class="btn btn-primary">'.JText::_('COM_N3TTEMPLATE_POSTINSTALL_MESSAGES_REVIEW').'</a></p>';							
		    $html[] = '</div>';
        return implode("\n",$html);	        
      }  		
    } 
    return '';   
  }
}
