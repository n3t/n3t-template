<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

jimport('joomla.plugin.helper');

class n3tTemplateHelperPlugin extends JObject
{
  
	public static function loadPlugins() {
		static $plugins;
		if (!isset ($plugins)) {
  	  $db = JFactory::getDBO();
      $db->setQuery('SELECT element AS plugin, params, enabled AS published, name FROM #__extensions WHERE type="plugin" AND folder="n3ttemplate"');
      $plugins = $db->loadObjectList('plugin');
      $lang = JFactory::getLanguage();
      foreach ($plugins as & $plugin) {
        $lang->load('plg_n3ttemplate_'.$plugin->plugin,JPATH_ADMINISTRATOR);
        $plugin->name = JText::_($plugin->name);
        $plugin->name = preg_replace('/^n3ttemplate\s*-?\s*/i', '', $plugin->name);
      }
		}
		return $plugins;  	    
	}
  
	public static function loadItems($plugin, $params) {
	  $items = array();
	  JPluginHelper::importPlugin('n3ttemplate');
    $_params = new JRegistry();
    $_params->loadString($params);
    $dispatcher = JDispatcher::getInstance();
    $dispatcher->trigger('onN3tTemplateItems', array ($plugin, & $items, $_params));
    return $items;    	    
	}

	public static function loadTemplate($plugin, $params) {    
	  $template = '';
	  JPluginHelper::importPlugin('n3ttemplate');
    $dispatcher = JDispatcher::getInstance();
    $_params = new JRegistry();
    $_params->loadString($params);
    $dispatcher->trigger('onN3tTemplateTemplate', array ($plugin, & $template, $_params));
    return $template; 	   
	}
	
}
