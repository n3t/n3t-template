ALTER TABLE #__n3ttemplate_categories
  CHANGE checked_out checked_out INT( 10 ) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE #__n3ttemplate_categories
  CHANGE access access INT( 10 ) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE #__n3ttemplate_templates
  CHANGE checked_out checked_out INT( 10 ) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE #__n3ttemplate_templates
  CHANGE access access INT( 10 ) UNSIGNED NOT NULL DEFAULT 0;
