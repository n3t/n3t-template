<?php
/**
 * @package n3t Template
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

JRequest::setVar('view', JRequest::getCmd('view','templates')); 

require_once(JPATH_COMPONENT_ADMINISTRATOR.'/controller.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.'/model.php');

require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/button.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/html.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/plugin.php');

$controller = JRequest::getCmd('view','templates');
$path = JPATH_COMPONENT_ADMINISTRATOR.'/controllers/'.$controller.'.php';
if(JFile::exists($path))
{
	require_once($path);
}
else 
{
	JError::raiseError('500',JText::_('COM_N3TTEMPLATE_UNKNOWN_CONROLLER').' '.$controller);
}

$controllerName='n3tTemplateController'.ucfirst($controller);

$class=new $controllerName();
$class->execute(JRequest::getCmd('task'));
$class->redirect();
