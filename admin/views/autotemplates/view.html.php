<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class n3tTemplateViewAutotemplates extends JViewLegacy 
{
  protected $data;
  protected $pagination;
  protected $lists;
  protected $user;  

	function display($tpl=null) 
  {
		n3tTemplateHelperHTML::toolbarTitle(JText::_('COM_N3TTEMPLATE_AUTOTEMPLATES'));
		n3tTemplateHelperHTML::subtoolbar('autotemplates');
		
		$this->data = $this->get('data');
		$this->lists = $this->get('lists');
    $this->pagination = $this->get('pagination');
    $this->user = JFactory::getUser();        
    
    $this->addToolbar();
    $this->sidebar = JHtmlSidebar::render();
		
		parent::display($tpl);
	}
  
	protected function addToolbar()
	{
  	JToolBarHelper::custom('delete','delete.png','delete_f2.png', 'COM_N3TTEMPLATE_DELETE', true);
    
 		JToolBarHelper::divider();
		if ($this->user->authorise('core.admin', 'com_n3ttemplate'))
			JToolbarHelper::preferences('com_n3ttemplate');
    JToolBarHelper::help('autotemplates',true);    

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_CATEGORY'),
			'category_id',
      JHtml::_('select.options', n3tTemplateHelperHTML::categoryTreeOptions(), 'value', 'text', $this->lists["filter_category"], true)      			
		);

		JHtmlSidebar::addFilter(
			JText::_('COM_N3TTEMPLATE_SELECT_ARTICLE_POSITION'),
			'position',
      JHtml::_('select.options', n3tTemplateHelperHTML::filterPositionOptions(false), 'id', 'title', $this->lists["filter_position"], true)      			
		);
  }  	
  	
}
