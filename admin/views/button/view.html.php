<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class n3tTemplateViewButton extends JViewLegacy
{
	public function __construct()
	{
		parent::__construct();
		$this->addHelperPath(JPATH_COMPONENT_ADMINISTRATOR.'/helpers');
		$this->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR.'/views/'.$this->getName().'/tmpl');
	}

	function display($tpl = null)
	{    
	  $this->loadHelper('html');
	  n3tTemplateHelperHTML::assetsPopup();
	  
	  $params = JComponentHelper::getParams( 'com_n3ttemplate' ); 	  
	  $this->assignRef('params',$params);
     				
		parent::display($tpl);
	}
}