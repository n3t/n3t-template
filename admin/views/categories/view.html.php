<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class n3tTemplateViewCategories extends JViewLegacy 
{
  protected $data;
  protected $pagination;
  protected $lists;
  protected $user;  

	function display($tpl=null) 
  {  
		n3tTemplateHelperHTML::toolbarTitle(JText::_('COM_N3TTEMPLATE_CATEGORIES'));
		n3tTemplateHelperHTML::subtoolbar('categories');
         
		$this->data = $this->get('data');
		$this->lists = $this->get('lists');
    $this->pagination = $this->get('pagination');
    $this->user = JFactory::getUser();        
    			    		
    $this->addToolbar();
    $this->sidebar = JHtmlSidebar::render();
    		
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		if ($this->lists['filter_state'] > -2) {
  	  JToolBarHelper::publishList();
  	  JToolBarHelper::unpublishList();
      JToolBarHelper::divider();
    	JToolbarHelper::addNew('edit');
      JToolBarHelper::custom('copy','copy.png','copy_f2.png', 'COM_N3TTEMPLATE_COPY', true);
			JToolbarHelper::editList();
      JToolBarHelper::divider();
			JHtml::_('bootstrap.modal', 'collapseModal');			
      $layout = new JLayoutFile('joomla.toolbar.batch');
			JToolBar::getInstance('toolbar')->appendButton('Custom', $layout->render(array('title' => JText::_('JTOOLBAR_BATCH'))), 'batch');            
			JToolbarHelper::trash();
		} else {
  		JToolBarHelper::custom('restore','undo.png','undo_f2.png', 'COM_N3TTEMPLATE_RESTORE', true);
	   	JToolBarHelper::custom('delete','delete.png','delete_f2.png', 'COM_N3TTEMPLATE_DELETE', true);
    }
    JToolBarHelper::divider();
		if ($this->user->authorise('core.admin', 'com_n3ttemplate'))
			JToolbarHelper::preferences('com_n3ttemplate');    
    JToolBarHelper::help('categories',true);  

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_state',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('archived' => false, 'all'  => false)), 'value', 'text', $this->lists["filter_state"], true)
		);
  }  	
		
}
