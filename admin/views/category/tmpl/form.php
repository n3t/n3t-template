<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JHTML::_('behavior.modal'); 
JHTML::_('behavior.formvalidation');
JHTML::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}

    if (document.formvalidator.isValid(form)) {
			submitform( pressbutton );
		} else {
		  var msg = "<?php echo JText::_( 'COM_N3TTEMPLATE_INPUT_ERRORS', true ); ?>\n";
		  if($('title').hasClass('invalid')){msg += "\n\t* <?php echo JText::_( 'COM_N3TTEMPLATE_INPUT_EMPTY_TITLE', true ); ?>";}
		  alert(msg);
    }
	}
	
	function checkPluginParams() {
    jQuery('#myTabTabs a[href^="#plugin-"]').parent().css('display','none');
    var plugin, use_plugin;
    plugin = jQuery('#plugin').val();
    use_plugin = jQuery('#use_plugin_params').prop('checked');    
	  jQuery('#use_plugin_params').prop('disabled', !plugin);
    if (plugin && use_plugin)
      jQuery('#myTabTabs a[href^="#plugin-'+plugin+'-"]').parent().css('display','initial');      
  }
  
  jQuery(function() {
    checkPluginParams(); 
    jQuery('#plugin').change(checkPluginParams);
  });
</script> 
<form action="<?php echo JRoute::_('index.php?option=com_n3ttemplate'); ?>" method="post" name="adminForm" id="adminForm">
<div class="form-inline form-inline-header">
	<label for="title" title="<?php echo JText::_( 'COM_N3TTEMPLATE_TITLE_DESC' ); ?>">
		<?php echo JText::_( 'COM_N3TTEMPLATE_TITLE' ); ?>:
	</label>
  <input class="input-xxlarge input-large-text required" type="text" name="title" id="title" size="50" maxlength="250" value="<?php echo $this->data->title;?>" />
</div>
<div class="form-horizontal">
  <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>
  <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_N3TTEMPLATE_DETAILS', true)); ?>
		<div class="row-fluid">
			<div class="span9">      
          <div class="control-group">
            <div class="control-label">
      				<label for="parent">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_PARENT' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo n3tTemplateHelperHTML::categoryTree( $this->data->parent_id, 'parent_id', $this->data->id ); ?>
            </div>
          </div>                

          <div class="control-group">
            <div class="control-label">
      			  <label for="published" title="<?php echo JText::_( 'COM_N3TTEMPLATE_PUBLISHED_DESC' ); ?>">
      				  <?php echo JText::_( 'COM_N3TTEMPLATE_PUBLISHED' ); ?>:
      				</label>              
            </div>
            <div class="controls">
              <?php echo $this->lists['published']; ?>
            </div>
          </div>                

          <div class="control-group">
            <div class="control-label">
      			  <label for="access" title="<?php echo JText::_( 'COM_N3TTEMPLATE_ACCESS_DESC' ); ?>">
      				  <?php echo JText::_( 'COM_N3TTEMPLATE_ACCESS' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['access']; ?>
            </div>
          </div>                

          <div class="control-group">
            <div class="control-label">
      				<label for="ordering" title="<?php echo JText::_( 'COM_N3TTEMPLATE_ORDERING_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_ORDERING' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['ordering']; ?>
            </div>
          </div>                

          <div class="control-group">
            <div class="control-label">
      				<label for="plugin" title="<?php echo JText::_( 'COM_N3TTEMPLATE_PLUGIN_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_PLUGIN' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['plugin']; ?>
            </div>
          </div>                

          <div class="control-group">
            <div class="control-label">
      				<label for="use_plugin_params" title="<?php echo JText::_( 'COM_N3TTEMPLATE_USE_PLUGIN_PARAMS_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_USE_PLUGIN_PARAMS' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <input class="" type="checkbox" name="use_plugin_params" id="use_plugin_params"<?php echo $this->data->plugin_params ? ' checked="checked"' : '';?><?php echo $this->data->plugin ? '' : ' disabled="disabled"';?> onchange="checkPluginParams();" />
            </div>
          </div>                

          <div class="control-group">
            <div class="control-label">
      				<label for="note" title="<?php echo JText::_( 'COM_N3TTEMPLATE_NOTE_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_NOTE' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <input class="input-large" type="text" name="note" id="note" size="50" maxlength="250" value="<?php echo $this->data->note;?>" />          
            </div>
          </div>                
			</div>
			<div class="span3">
      </div>
    </div>
  <?php echo JHtml::_('bootstrap.endTab'); ?>  
  <?php $this->renderParams();?>  
  <?php echo JHtml::_('bootstrap.endTabSet'); ?>    
</div>      
	<input type="hidden" name="option" value="com_n3ttemplate" />
	<input type="hidden" name="cid[]" value="<?php echo $this->data->id; ?>" />
	<input type="hidden" name="view" value="categories" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>