<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JHTML::_('behavior.modal'); 
JHTML::_('behavior.formvalidation');  
JHTML::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
    
    var text = <?php echo $this->editor->getContent( 'template' ); ?> 
    if (document.formvalidator.isValid(form) && text != '') {
			<?php echo $this->editor->save( 'template' ) ; ?> 		
			submitform( pressbutton );
		} else {
		  var msg = "<?php echo JText::_( 'COM_N3TTEMPLATE_INPUT_ERRORS', true ); ?>\n";
		  if($('title').hasClass('invalid')){msg += "\n\t* <?php echo JText::_( 'COM_N3TTEMPLATE_INPUT_EMPTY_TITLE', true ); ?>";}
		  if(text == ''){msg += "\n\t* <?php echo JText::_( 'COM_N3TTEMPLATE_INPUT_NO_CONTENT', true ); ?>";}
		  alert(msg);
    }
	}
</script> 
<form action="<?php echo JRoute::_('index.php?option=com_n3ttemplate'); ?>" method="post" name="adminForm" id="adminForm">
<div class="form-inline form-inline-header">
	<label for="title" title="<?php echo JText::_( 'COM_N3TTEMPLATE_TITLE_DESC' ); ?>">
		<?php echo JText::_( 'COM_N3TTEMPLATE_TITLE' ); ?>:
	</label>
  <input class="input-xxlarge input-large-text required" type="text" name="title" id="title" size="50" maxlength="250" value="<?php echo $this->data->title;?>" />
</div>
  
<div class="form-horizontal">
  <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>
  <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_N3TTEMPLATE_DETAILS', true)); ?>
		<div class="row-fluid">
			<div class="span9">
        <?php echo $this->editor->display( 'template',  htmlspecialchars($this->data->template, ENT_QUOTES), '100%', '500', '60', '20' ) ; ?>
			</div>
			<div class="span3">
          <div class="control-group">
            <div class="control-label">
      				<label for="category" title="<?php echo JText::_( 'COM_N3TTEMPLATE_CATEGORY_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_CATEGORY' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo n3tTemplateHelperHTML::categoryTree( $this->data->category_id, 'category_id' ); ?>
            </div>
          </div>
  
          <div class="control-group">
            <div class="control-label">
      			  <label for="published" title="<?php echo JText::_( 'COM_N3TTEMPLATE_PUBLISHED_DESC' ); ?>">
      				  <?php echo JText::_( 'COM_N3TTEMPLATE_PUBLISHED' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['published']; ?>
            </div>
          </div>
          
          <div class="control-group">
            <div class="control-label">
      			  <label for="access" title="<?php echo JText::_( 'COM_N3TTEMPLATE_ACCESS_DESC' ); ?>">
      				  <?php echo JText::_( 'COM_N3TTEMPLATE_ACCESS' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['access']; ?>
            </div>
          </div>
  
          <div class="control-group">
            <div class="control-label">
      			  <label for="display_access" title="<?php echo JText::_( 'COM_N3TTEMPLATE_DISPLAY_ACCESS_DESC' ); ?>">
      				  <?php echo JText::_( 'COM_N3TTEMPLATE_DISPLAY_ACCESS' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['display_access']; ?>          
            </div>
          </div>
  
          <div class="control-group">
            <div class="control-label">
      				<label for="ordering" title="<?php echo JText::_( 'COM_N3TTEMPLATE_ORDERING_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_ORDERING' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <?php echo $this->lists['ordering']; ?>
            </div>
          </div>
  
          <div class="control-group">
            <div class="control-label">
      				<label for="note" title="<?php echo JText::_( 'COM_N3TTEMPLATE_NOTE_DESC' ); ?>">
      					<?php echo JText::_( 'COM_N3TTEMPLATE_NOTE' ); ?>:
      				</label>            
            </div>
            <div class="controls">
              <input class="input-large" type="text" name="note" id="note" size="50" maxlength="250" value="<?php echo $this->data->note;?>" />          
            </div>
          </div>        
      
        <p class="alert alert-info"><?php echo JText::_( 'COM_N3TTEMPLATE_AUTOTEMPLATE_CONTENT_CATEGORIES' ); ?></p>
        <?php foreach($this->lists['autotemplates'] as $position => $categories) { ?>
        <div class="control-group">
          <div class="control-label">
        		<label for="" title="<?php echo JText::_( 'COM_N3TTEMPLATE_ARTICLE_POSITION_'.strtoupper($position).'_DESC' ); ?>">
        			<?php echo JText::_( 'COM_N3TTEMPLATE_ARTICLE_POSITION_'.strtoupper($position) ); ?>:
        		</label>            
          </div>
          <div class="controls">
            <?php echo $categories; ?>
          </div>
        </div>      
        <?php } ?>        				
			</div>
		</div>  
  <?php echo JHtml::_('bootstrap.endTab'); ?>  
  <?php $this->renderParams();?>  
  <?php echo JHtml::_('bootstrap.endTabSet'); ?>
</div>
	<input type="hidden" name="option" value="com_n3ttemplate" />
	<input type="hidden" name="cid[]" value="<?php echo $this->data->id; ?>" />
	<input type="hidden" name="view" value="templates" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>