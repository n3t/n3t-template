<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class n3tTemplateViewTemplate extends JViewLegacy 
{
  protected $data;
  protected $lists;
  protected $editor;    

	function display($tpl=null) 
  {
    $user	= JFactory::getUser();
    $model = $this->getModel();
    $app = JFactory::getApplication();
          	
		$this->data = $this->get('data');
    $this->editor = JFactory::getEditor();
		
		if ($this->data->id) 
		  n3tTemplateHelperHTML::toolbarTitle($this->data->title);
		else		
		  n3tTemplateHelperHTML::toolbarTitle(JText::_('COM_N3TTEMPLATE_NEW_TEMPLATE'));
		
		if ($model->isCheckedOut( $user->get('id') )) {
			$msg = JText::_( 'COM_N3TTEMPLATE_BEING_EDITED' );
			$app->redirect( 'index.php?option=com_n3ttemplate', $msg );
		}
    		
		$this->lists = array();
		$query = 'SELECT ordering AS value, title AS text'
			. ' FROM #__n3ttemplate_templates'
			. ' WHERE category_id='.$this->data->category_id
			. ' AND published>=0'
			. ' ORDER BY ordering';		
    $this->lists['ordering'] 			= n3tTemplateHelperHTML::ordering($this->data, $this->data->id, $query);  
    $this->lists['published'] 		= n3tTemplateHelperHTML::booleanList('published', $this->data->published );
    $this->lists['access'] 		    = n3tTemplateHelperHTML::accesslevel( $this->data->access );
    $this->lists['display_access']= n3tTemplateHelperHTML::accesslevel( $this->data->display_access, 'display_access' );
    $this->lists['autotemplates'] = array();
    foreach(n3tTemplateHelperHTML::getArticlePositions() as $position)
      $this->lists['autotemplates'][$position] = n3tTemplateHelperHTML::contentCategories('autotemplates['.$position.']', $this->data->autotemplates[$position] );
    
    JFilterOutput::objectHTMLSafe( $this->data, ENT_QUOTES, 'template' );
       			        			
    $this->addToolbar();
    
		parent::display($tpl);
	}
	
	function renderParams() {
	  $form = $this->get('Form');
	  if ($form && $this->data->params) {
		  $temp = new JRegistry;
		  $temp->loadString($this->data->params);
      $form->bind($temp);
	  }    
	  $fieldSets = $form->getFieldsets();
		foreach ($fieldSets as $name => $fieldSet) {			
      echo JHtml::_('bootstrap.addTab', 'myTab', $name.'-options', JText::_($fieldSet->label, true));
			if (isset($fieldSet->description) && trim($fieldSet->description)) { ?>
				<p class="alert alert-info"><?php echo $this->escape(JText::_($fieldSet->description));?></p>
			<?php } ?>
			<div class="form-horizontal">
				<?php foreach ($form->getFieldset($name) as $field) { ?>
  				<div class="control-group">
  					<div class="control-label">
              <?php echo $field->label; ?>
            </div>  
            <div class="controls">
              <?php echo $field->input; ?>
            </div>
  				</div>
				<?php } ?>
			</div>
		<?php
      echo JHtml::_('bootstrap.endTab');
    }    
	}

	protected function addToolbar()
	{
		JToolBarHelper::apply();
	  JToolBarHelper::save();
    JToolBarHelper::save2new();
    JToolBarHelper::save2copy();
	  JToolBarHelper::divider();
    JToolBarHelper::cancel();
    JToolBarHelper::divider();
    JToolBarHelper::help('template',true);    			
  }
  
}
