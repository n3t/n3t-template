<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JHTML::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');

$lists = $this->lists;
?>
<form action="<?php echo JRoute::_('index.php?option=com_n3ttemplate'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) { ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php } else { ?>
	<div id="j-main-container">
<?php } ?>
  <?php echo n3tTemplateHelperHTML::postInstallMessages(); ?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_search" class="element-invisible"><?php echo JText::_('JSEARCH_FILTER_LABEL');?></label>
				<input type="text" name="search" id="search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo htmlspecialchars($lists['search']);?>" class="hasTooltip" title="<?php echo JHtml::tooltipText('COM_N3TTEMPLATE_FILTER_DESC'); ?>" />
			</div>
			<div class="btn-group pull-left">
				<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.getElementById('search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
		</div>
		<div class="clearfix"> </div>
	<?php if (empty($this->data)) { ?>
		<div class="alert alert-no-items">
			<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
		</div>
  <?php } else { ?>
    
	<table class="table table-striped" id="templateList">
		<thead>
			<tr>
				<th width="1%" class="center">
					<?php echo JHtml::_('grid.checkall'); ?>
				</th>
				<th>
  				<?php echo JHTML::_('grid.sort',  'COM_N3TTEMPLATE_TITLE', 't.title', @$lists['order_Dir'], @$lists['order'] ); ?>				
				</th>				
				<th width="15%" class="nowrap">
				  <?php echo JHTML::_('grid.sort',  'COM_N3TTEMPLATE_CATEGORY', 'c.title', @$lists['order_Dir'], @$lists['order'] ); ?>
        </th> 
        <?php if ($lists["filter_state"]>-2) { ?>
				<th width="5%" class="nowrap center">
				  <?php echo JHTML::_('grid.sort',  'COM_N3TTEMPLATE_PUBLISHED', 't.published', @$lists['order_Dir'], @$lists['order'] ); ?>
        </th>         
				<th width="10%" class="nowrap center hidden-phone">
				  <?php echo JHTML::_('grid.sort',  'COM_N3TTEMPLATE_ACCESS', 't.access', @$lists['order_Dir'], @$lists['order'] ); ?>
        </th>         
				<th width="10%" class="nowrap center hidden-phone">
				  <?php echo JHTML::_('grid.sort',  'COM_N3TTEMPLATE_DISPLAY_ACCESS', 't.display_access', @$lists['order_Dir'], @$lists['order'] ); ?>
        </th>         
				<th width="10%" class="nowrap hidden-phone">
				  <?php echo JHTML::_('grid.sort', 'COM_N3TTEMPLATE_ORDERING', 't.ordering', @$lists['order_Dir'], @$lists['order'] ); ?>
				  <?php echo JHTML::_('grid.order',  $this->data ); ?>
				</th>
				<?php } ?>
        <th width="1%" class="nowrap center hidden-phone">
				  <?php echo JText::_('COM_N3TTEMPLATE_ID'); ?>
				</th>
		</thead>
		<tbody>
		<?php
			$k=0;
			for($i=0;$i<count($this->data);$i++) {
				$row=$this->data[$i];
				
				$link='index.php?option=com_n3ttemplate&view=templates&task=edit&cid[]='.$row->id;
				?>
				<tr class="row<?php echo $k; ?>">
					<td class="center"><?php echo JHTML::_('grid.checkedout',$row,$i); ?></td>
					<td><?php if(($row->checked_out == 0 || $row->checked_out == $this->user->get('id')) && $lists["filter_state"]!=-2) {
						?>
						<a href="<?php echo $link; ?>"><?php echo $row->title; ?></a>
						<?php
					} else {
						echo $row->title;
					}
          if ($row->note) { 
          ?>
          <div class="small"><?php echo $row->note; ?></div>
          <?php
          }          					
					?></td>
					<td class="nowrap"><?php echo $row->category_title; ?></td>
					<?php if ($lists["filter_state"]>-2) { ?>
					<td class="nowrap center"><?php echo JHTML::_('grid.published',$row,$i); ?></td>
					<td class="nowrap center hidden-phone"><?php echo n3tTemplateHelperHTML::displayAccess($row->access,$row->groupname); ?></td>
					<td class="nowrap center hidden-phone"><?php echo n3tTemplateHelperHTML::displayAccess($row->display_access,$row->display_groupname); ?></td>
					<td class="nowrap hidden-phone">
							<?php if (@$lists['order'] == 't.ordering') { ?>
								<div class="input-prepend">
									<?php if (@$lists['order_Dir'] == 'asc') { ?>
										<span class="add-on"><?php echo $this->pagination->orderUpIcon($i, $row->category_id == @$this->data[$i-1]->category_id, 'orderup', 'JLIB_HTML_MOVE_UP', $lists['ordering']); ?></span>
										<span class="add-on"><?php echo $this->pagination->orderDownIcon($i, count($this->data), $row->category_id == @$this->data[$i+1]->category_id, 'orderdown', 'JLIB_HTML_MOVE_DOWN', $lists['ordering']); ?></span>
									<?php } elseif (@$lists['order_Dir'] == 'desc') { ?>
										<span class="add-on"><?php echo $this->pagination->orderUpIcon($i, $row->category_id == @$this->data[$i-1]->category_id, 'orderdown', 'JLIB_HTML_MOVE_UP', $lists['ordering']); ?></span>
										<span class="add-on"><?php echo $this->pagination->orderDownIcon($i, count($this->data), $row->category_id == @$this->data[$i+1]->category_id, 'orderup', 'JLIB_HTML_MOVE_DOWN', $lists['ordering']); ?></span>
									<?php } ?>
									<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="width-20 text-area-order" />
								</div>
							<?php 
                } else { 
                  echo $row->ordering; 
                } 
              ?>                        
					</td>           					
					<?php } ?>
					<td class="nowrap center hidden-phone"><?php echo $row->id; ?></td>
				</tr>
				<?php
				$k=1-$k;
			}
		?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="<?php echo $lists["filter_state"]>-2 ? 8 : 3; ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>	
		</tfoot>
	</table>
  <?php } ?>
  <?php echo $this->loadTemplate('batch'); ?>
</div>
<input type="hidden" name="option" value="com_n3ttemplate" />
<input type="hidden" name="view" value="templates" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="hidemainmenu" value="0" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" /> 
<?php echo JHTML::_('form.token'); ?>
</form>