<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$lists = $this->lists;
if ($lists["filter_state"]>-2) { ?>
<div class="modal hide fade" id="collapseModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&#215;</button>
		<h3><?php echo JText::_('COM_N3TTEMPLATE_BATCH_OPTIONS'); ?></h3>
	</div>
  <div class="modal-body modal-batch">
    <div class="row-fluid">
  		<div class="control-group span6">
  			<label class="control-label" for="batch-access" title="<?php echo JText::_( 'COM_N3TTEMPLATE_BATCH_ACCESS_DESC' ); ?>">
  				<?php echo JText::_( 'COM_N3TTEMPLATE_BATCH_ACCESS' ); ?>:
  			</label>
  			<div class="controls">
  				<?php echo $lists['batch-access']; ?>				
  			</div>
  		</div>

  		<div class="control-group span6">
  			<label class="control-label" for="batch-display-access" title="<?php echo JText::_( 'COM_N3TTEMPLATE_BATCH_DISPLAY_ACCESS_DESC' ); ?>">
  				<?php echo JText::_( 'COM_N3TTEMPLATE_BATCH_DISPLAY_ACCESS' ); ?>:
  			</label>
  			<div class="controls">
  				<?php echo $lists['batch-display-access']; ?>				
  			</div>
  		</div>
    </div>
    
    <div class="row-fluid">
      <div class="control-group span6">
  			<label class="control-label" for="batch-category-id" title="<?php echo JText::_( 'COM_N3TTEMPLATE_BATCH_MOVECOPY_DESC' ); ?>">
  				<?php echo JText::_( 'COM_N3TTEMPLATE_BATCH_MOVECOPY' ); ?>:
  			</label>
  			<div class="controls">
  				<?php echo $lists['batch-category-id']; ?>				
  			</div>
  			<?php echo $lists['batch-movecopy']; ?>
  		</div>
    </div>	

  	<div class="modal-footer">
    	<button type="button" class="btn" onclick="document.getElement('select[name=batch[category_id]]').value=-1;document.getElement('select[name=batch[access]]').value='';document.getElement('select[name=batch[display_access]]').value='';"  data-dismiss="modal">
    		<?php echo JText::_('JCANCEL'); ?>
    	</button>
    	<button type="button" class="btn btn-primary" onclick="Joomla.submitbutton('batch');">
    		<?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
    	</button>
    </div>
  </div>
</div>    	 
<?php } 	
