License
=======

n3t Template is released under [GNU/GPL v3][GNUGPL] license.

[GNUGPL]: http://www.gnu.org/licenses/gpl-3.0.html