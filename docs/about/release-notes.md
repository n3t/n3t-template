Release notes
=============

Next release
------------

 * Editor button - icon display corrected
 * Automatic editor templates - MooTools dependency removed, Bootstrap confirmation dialog
 * Automatic templates for category and featured views  

1.8.x
-----

#### 1.8.1

 * File and Folder plugins malfunction solved
 * Module, Position and Phoca Maps plugins malfunction on sites with enabled error 
   reporting solved
 * Youtube plugin temporarily deprecated due the deprecation of Youtube API v2

#### 1.8.0

 * Joomla! 1.x support discontinued
 * Joomla! 2.5 support discontinued
 * Joomla! 3.4 support
 * Upgrade notification icon and functionality removed, rely on standard Joomla 
   functions
 * AcePolls plugin support discontinued
 * DJ Image Slider plugin support discontinued
 * Credits and Donation module removed
 * Administrator interface redesigned
 * Code improvements
 * Only English translation contained in installation package

1.7.x
-----

#### 1.7.7

 * New plugin DJ Image Slider - allows choosing of category from DJ Image Slider
   component
 * Folder plugin - DJ Image Slider support added
 * Automatic templates - new way of loading automatic template
 * Checked out items - wrong display of checked out items in list
 * Items Check out - works wrong for users IDs over 127

#### 1.7.6

 * Phoca Maps plugin - wrong ordering
 * Automatic templates - wrong templates were loaded in editor, when other automatic
   templates position were used
 * Getting Joomla! 2.5 ready
 * New plugin AcePolls - allows choosing of poll from AcePolls component
 * Folder plugin - Mp3 Browser support added

#### 1.7.5

 * Youtube plugin - user uploads displayed instead of user favorites
 * Youtube plugin - for long names and descriptions only part was displayed
 * User interface J15! - correct button names in category edit

#### 1.7.4

 * User interface improvements

#### 1.7.3

 * Help added
 * User interface improvements
 * JCE editor wrong behavior solved

#### 1.7.2

 * Automatic templates can be know loaded in editor, or automatically added at 
   the beginning or end of article
 * New plugin File - allows choosing of file
 * New plugin Folder - allows choosing of folder
 * New plugin Phoca Maps - allows choosing of map from Phoca Maps component
 * Displaying automatic template in editor is now influenced by Display Access Level
   property of item
 * Displaying template inserted as link to article is now influenced by Display 
   Access Level property of item

#### 1.7.1

 * Added support for indirect inserting (Insert as link). Template is replaced 
   when article is displayed.
 * Added support for automatic loading templates for content categories.
 * New plugin Module - allows to choose module
 * Position plugin - custom code option added
 * Position plugin - Modules Anywhere support added
 * Youtube plugin - custom code functionality and help revised
 * Removed wrong character encoding on servers with preset default encoding.
 * German translation added (thanks to M. Cigdem Cebe)

#### 1.7.0

 * Plugins support added
 * New plugin Position - allows to choose template position
 * New plugin Youtube - allows to choose video from server youtube.com
 * Small code and functionality improvements

1.6.x
-----

#### 1.6.2

 * Solved JavaScript error for Joomla! 1.5 with disabled Mootools Upgrade plugin

#### 1.6.1

 * Solved JavaScript error in Google Chrome browser

#### 1.6.0

 * Added Joomla! 1.6 and 1.7 support
 * Media files moved to media folder
 * Button access level definition changed to Joomla! standard
 * Possibility to define access level for categories and templates
 * Removed malfunctionality while editing in frontend with SEF enabled