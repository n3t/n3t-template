Automatic templates
===================

How to Access
-------------

In n3tTemplate [Control panel](cpanel.md) click on Automatic templates icon, or
anywhere click on Automatic templates link on toolbar.

--------------------------------------------------------------------------------

Description
-----------

This is an overview of automatic templates. Here you can find, filter and delete
automatic templates for content categories. If you want to add automatic template
for specific category, please use the [edit function of template](template.md).

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/autotemplates/screenshot.jpg)

--------------------------------------------------------------------------------

Column headers
--------------

#### &#35;
This is just number of row in the list. It has informational character.

#### Content category
In this column you can see Title of the content category.

#### Position
Position, where template automatically loads. It could be: 

#### Editor
Such template loads for chosen category in the editor, when creating new article.

#### Above article
Such template loads at the beginning of article, when full article text is displayed.

#### Below article
Such template loads at the end of article, when full article text is displayed.

#### Above article intro
Such template loads at the beginning of article, when inly intro article text
(the part before read more link) is displayed.

#### Below article intro
Such template loads at the end of article, when inly intro article text
(the part before read more link) is displayed.

#### Template
Title of the template. If Template has filled optional field Note, it will be visible here also.

#### Category
Title of category of template.

--------------------------------------------------------------------------------

List filters
------------

![Search](images/autotemplates/filter-search.jpg)

On the left side above the list you can specify filter. Enter the text, you are
searching for, and press the button Apply. Only rows containing entered text in
Title, Note or Template field of template or in title of content category will be
displayed. If you want to reset the filter (display all rows), just press the button Reset.

![Filter](images/autotemplates/filter.jpg)

On the right side above the list of categories, you can choose which template
category to display and/or which position to display.

--------------------------------------------------------------------------------

Toolbar
-------

On the toolbar you can find following icons:

![Toolbar](images/autotemplates/toolbar.jpg)

#### Delete
Permanently deletes selected rows. This doesn't delete template or content category,
it just deletes automatic templating for the selected rows. Select rows by checking
the box in the second column.

#### Help
Will display help dialog.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Inserting and editing templates](template.md) 