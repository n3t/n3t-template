Editor button
=============

How to Access
-------------

If n3tTemplate editors-xtd plugin is enabled, under any editor will be displayed
additional button called Templates. Just click on this button. Note, that if the
Access level of the plugin is set to something else than public, only authorized
users can see the button.

![Editor button](images/button/button.jpg)

--------------------------------------------------------------------------------

Description
-----------

Editor button allows authors to use prepared templates to insert into the text.
Clicking on button opens popup window, containingtree of categories and templates
defined in [Category manager](categories.md) and [Template manager](templates.md). 
By choosing template in the tree, preview of the template canbe load in the bottom
part of the window, if it is enabled in [Configuration](config.md).

For inserting the template into the text, just click one of the buttons at the top.

#### Insert as link
In the editor is inserted just simple code something like `{n3ttemplate 1}`. This
code is replaced upon displaying the article in the frontend to the user by proper
template.

#### Insert template
Inserts the code of the template directly into the editor, as is displayed in the
preview window. The code should be inserted in the current position of cursor, but
this is influenced by editor itself, n3tTemplate cannot influence this.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/button/screenshot.jpg)

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Category manager](categories.md) 
  * [Inserting and editing category](category.md) 
  * [Template manager](templates.md) 
  * [Inserting and editing template](template.md) 
  * [Configuration](config.md) 