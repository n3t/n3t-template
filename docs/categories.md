Category manager
================

How to Access
-------------

In the n3tTemplate [Control panel](cpanel.md) click on icon Categories, or whenever click on 
toolbar link Categories.

--------------------------------------------------------------------------------

Description
-----------

Category manager allows you to create, delete and organize categories. Categories 
can contain [templates](templates.md) or can work as [plugin](plugins.md) placeholder.

Categories can be organized in the tree - every category can have parent category, 
and in the [editor buttton](button.md) will appear as a child node of such category. 
More over, every category can be published or unpublished, have specified Access level, 
and of course has some title.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/categories/screenshot.jpg)

--------------------------------------------------------------------------------

Column headers
--------------

#### &#35;
This is just number of row in the list of categories. It has informational character.

#### Title
In this column you can see Title of the category, and if the category has filled
optional field note, it will be visible here also.

#### Plugin
If category uses plugin, here will be displayed its Title and name. Such category
couldn't contain any subcategories nor templates.

#### Published
If category is published (visible to users) in this column will appear green checkmark.
If unpublished, there will be red cross. Clicking on the icon will switch the state
of category from published to unpublished or vice versa.

#### Plugin enabled
If category uses plugin, in this column will be green checkamerk if the plugin is
also publiished, or red cross, if the plugin is unpublished. It has informational
character. For proper functionality here should be always green checkmark for
published categories. If not, you can go to [Plugin manager](plugins.md) to publish
the right plugin.

#### Access
In this column is vissible neccessary Access level for users to see this category
in the [editor button](button.md) tree. For explanation of access levels please 
see the [Joomla! help][AccessLevels].

#### Ordering
In this column is visible ordering of category within its parent category.
If the list is sorted by this column, you can influence the ordering directly
by clicking up / down icons, or fill the correct oredreing numbers in the boxes
for every displayed row, and click the save icon in the heading of this column.

Note that ordering s specified within the parent category, so not in all rows you
can see the up / down icons.

#### Id
This is internal ID of the category.

--------------------------------------------------------------------------------

List filters
------------

![Search](images/categories/filter-search.jpg)

On the left side above the list of categories you can specify filter. Enter the text, 
you are searching for, and press the button Apply. Only categories containing eneterd 
text in Title or Note will be displayed. If you want to reset the filter (display all 
categories), just press the button Reset.

![Filter](images/categories/filter.jpg)

On the right side above the list of categories, you can choose which categories 
to display. If current is selected, all published and unpublished categories are
displayed.

If you choose published or unpublished, only published / unpublished
categories are displayed. If you choose trashed, only trashed categories are 
displayed.

Note that in trashed view, only some of columns are displayed.

--------------------------------------------------------------------------------

Toolbar
-------

On the toolbar you can find following icons:

![Toolbar](images/categories/toolbar.jpg)

#### Publish
Set chosen categories as published. Choose categories by checking the checkbox in
the second column in the list.

#### Unpublish
Set chosen categories as unpublished. Choose categories by checking the checkbox
in the second column in the list.

#### New
Opens dialog for inserting new category.

#### Copy
Creates a copy of all selected categories. Those copies will have all the properties
and settings same as the original category, and will be placed in the same parent
category. Choose categories by checking the checkbox in the second column in the list.

#### Edit
Edits chosen category. Select the category by checking the checkbox in the second
column in the list.

#### Trash
Trashes chosen categories. Choose categories by checking the checkbox in the second
column in the list. Note that this action will note delete the category from database,
it just mark the category as trashed. Trashed categories can be displayed by chossing
trashed in the list filter. If you trash category containing templates or other
categories, those will move to the root of the tree.

#### Help
Will display help dialog.

If you filter list to display only trashed categories, toolbar will contain these icons:

![Toolbar trashed](images/categories/toolbar-trashed.jpg)

#### Restore
Restores selected categories from trash. Restored categories will be restored at
the end of the root category (will not have parent category, ordering set to 9999,
and will be unpublished). Choose categories by checking the checkbox in the second
column in the list.

#### Delete
Completely removes the selected categories from the database. Choose categories
by checking the checkbox in the second column in the list.

--------------------------------------------------------------------------------

Batch operations
----------------

Using batch operations you can easily change some parameters of multiple categories.

![Batch operations](images/categories/batch.jpg)

#### Select Category for Move/Copy
Select from the list category where you want selected categories move or copy, and
select if you want to Move or Copy.

#### Set Access Level
Select new Access level for all selected categories.

To apply changes make your selections and click button Process.

--------------------------------------------------------------------------------

Tips
----

  * Click on the Title of a Category to edit it. 
  * Click on the green check mark or the red X in the Published column to toggle 
    between Published and Unpublished. 
  * Click on the Column Headers to sort Categories by that column. Click a second 
    time to sort descending (Z to A). Note that if you want to change categories 
    ordering, the list has to be ordered by ordering column. 

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Inserting and editing categories](category.md) 
  * [Usage of plugins](plugins.md) 
  * [AcePolls plugin](plugins/acepolls.md) 
  * [File plugin](plugins/file.md) 
  * [Folder plugin](plugins/folder.md) 
  * [Module plugin](plugins/module.md) 
  * [Phoca Maps plugin](plugins/phocamaps.md) 
  * [Position plugin](plugins/position.md) 
  * [Youtube plugin](plugins/youtube.md) 

[AccessLevels]: https://help.joomla.org/proxy/index.php?option=com_help&keyref=Help25:Users_Access_Levels