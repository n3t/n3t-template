Inserting and editing category
==============================

How to Access
-------------

In the [category manager](categories.md) click on toolbar icon New, or select category in the 
list and click on its title to edit it.

--------------------------------------------------------------------------------

Description
-----------

Categories can be used in two different ways.

As simple categories could contein other categories and/or templates, which allows 
to specify look and structure of [editor button](button.md) tree.

Other usage of categories is as Plugin container. If you choose in plugin field 
one of plugins, this category cannot contain other categories and/or templates, 
but will load its items in [editor button](button.md) tree dynamically according 
to plugin settings.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/category/screenshot.jpg)

--------------------------------------------------------------------------------

Fields
------

#### Title
Title of this category. It is not allowed to have duplicate titles within parent category.

#### Note
This note is for internall use, and will appear in the administration only.

#### Parent
Specify parent category of this category. In the [editor button(button.md) tree 
this category will appear as nested category within its parent. If you want to 
create category, which appears directly on the root, choose Uncategorized. 
Only simple categories (not plugin categories) can contain nested categories and/or 
templates.

#### Published
Specify, if this category is pulished or not. Unpublished categories are not visible 
in the [editor button](button.md) tree.

#### Access
Specify Access level of users, whose can see this category in the [editor button](button.md) tree.

#### Ordering
Specify ordering of this category within its parent category. When new category 
is being inserted, this field is not enabled. New categories are implicitly ordered
at the end of its parent category.

#### Plugin
If you want to use category as [plugin container](plugins.md) select here plugin to use. 
I f you plan this category to be just normal category (containing other categories 
and/or templates), leave this empty.

#### Override plugin settings
If plugin is selected in Field plugin, leave this field unchecked to use thats 
plugin default settings (can be influenced from [plugin edit page](plugins.md)). 
If you want to specify different settings just check this field, and in the left 
column under parameters will appear new parameters just for this category.

#### Id
This is internal Id of this category. It cannot be changed.

--------------------------------------------------------------------------------

Parameters
----------

#### Load expanded
If expanded is chosen, then this category will be displayed as expanded when 
initially loaded into category tree in [editor button](button.md).

--------------------------------------------------------------------------------

Toolbar
-------

On the toolbar you can find following icons:

![Toolbar](images/categories/toolbar.jpg)

#### Save
Saves changes and stay in the edit mode.

#### Save & close
Saves changes and close the edit mode. Goes back to [Category manager](categories.md).

#### Save & New
Saves changes and open page to insert another category.

#### Save as Copy
Saves changes as new category.

#### Cancel
Closes the edit form without saving changes and goes back to [Category manager](categories.md).

#### Help
Will display help dialog.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Category manager](categories.md) 
  * [Plugins overview](plugins.md) 

--------------------------------------------------------------------------------

Available plugins
-----------------

Here you can find detailed description of available plugins, and its usage.

  * [AcePolls plugin](plugins/acepolls.md) 
  * [File plugin](plugins/file.md) 
  * [Folder plugin](plugins/folder.md) 
  * [Module plugin](plugins/module.md) 
  * [Phoca Maps plugin](plugins/phocamaps.md) 
  * [Position plugin](plugins/position.md) 
  * [Youtube plugin](plugins/youtube.md) 