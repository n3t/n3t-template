Configuration
=============

How to Access
-------------

On n3tTemplate [Control panel](cpanel.md) click on Configuration icon.

--------------------------------------------------------------------------------

Description
-----------

This is the global configuration of n3tTemplate component. Here you can set basic
parameters, influencing the very basic functionality of n3tTemplate, and also define
rights to access this component.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/config/screenshot.jpg)

--------------------------------------------------------------------------------

Button settings
---------------

These settings influence the [editor button](button.md) behavior.

#### Access
Set the access level of users to see the [editor button]](button.md). Note that
this is the same as setiing access directly in n3tTemplate editors-xtd plugin in
Joomla! plugin manager.

#### Show preview
If set to true (default), in [editor button]](button.md) will be displayed preview
of selected template under the tree.

--------------------------------------------------------------------------------

Permissions
-----------

These permissions control who can access administration of n3tTemplate.

#### Administration
Access the administartion of n3tTemplate.

#### Configuration
Access the global configuration of n3tTemplate.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Control panel](cpanel.md) 