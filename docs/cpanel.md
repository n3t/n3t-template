Control panel
=============

How to Access
-------------

From outside of n3tTemplate go to menu Components and click on n3tTemplate.

When already using n3tTemplate, you can go back to Control panel anytime (when not 
editing item) by clicking the n3tTemplate text in the header just next to the icon, 
or by clicking the link Control panel on the toolbar.

--------------------------------------------------------------------------------

Description
-----------

This is the starting point of working with n3tTemplate. On the left side are displayed 
icons for basic actions, like displaying templates and categories, go to configuration etc. 

On the right side (if not disabled in Joomla modules) is displayed n3tTemplate 
credits module, with current version of n3tTemplate. Note that by clicking on 
the version number you can display release notes of n3tTemplate.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/cpanel/screenshot.jpg)