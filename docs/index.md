n3t Template
============

n3tTemplate component is content templating extension for Joomla. Some of it features are:

  * Define [templates](templates.md) (code snippets) in the backend, [categorize it](categories.md),
    publish it, asssign access level to it etc..
  * [Access](button.md) the tree of such defined templates by button displayed below 
    the editor of content article, and insert it to your editor. 
  * Define which template should [automatically load](autotemplates.md) to editor for
    which content category. 
  * Specify template which will be [automatically prepended or appended](autotemplates.md)
    to content article in specific category. 
  * Use powerfull [plugin system](plugins.md), to select other content than templates
    ([files](plugins/file.md), [folders](plugins/folder.md), or even [youtube videos](plugins/youtube.md)) 
    and insert links to it or 3rd party plugin codes to display it. 
  * Use [indirect templating](button.md) to display same code in many articles, and 
    still have chance to modify it on one place.
    
Notice
------

This documentation is based on n3t Template 1.7.7 on Joomla! 2.5. For Joomla! 3.x
is documentation in progress, however most of n3t Template parts didn't change so much, 
so only user interface looks different.      