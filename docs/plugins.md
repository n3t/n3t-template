Plugins overview
================

General description
-------------------

Plugins in n3tTemplate allows to display in the [editor button](button.md) tree
something else then just templates, and insert into article some extra code to
display it. For example [File plugin](plugins/file.md) allows to display files
from specified folder (for example images/videos), and inserts into the article
code for AllVideos plugin. It makes administrator life easier, and for newbies
without knowledge of Joomla! plugins usage, this is almost neccesary.

--------------------------------------------------------------------------------

Usage
-----

[Create new Category or edit existing one](category.md). In Plugin field choose
desired plugin, and check the box Override plugin settings, if you want to specify
settings just for this category. On the right side will appear few new parameters
groups - (usually Plugin - General settings and Plugin - Output settings), where
you can specify details on displaying items and inserting the code into the article.

--------------------------------------------------------------------------------

Available plugins
-----------------

Here you can find detailed description of available plugins, and its usage.

  * [AcePolls plugin](plugins/acepolls.md) 
  * [File plugin](plugins/file.md) 
  * [Folder plugin](plugins/folder.md) 
  * [Module plugin](plugins/module.md) 
  * [Phoca Maps plugin](plugins/phocamaps.md) 
  * [Position plugin](plugins/position.md) 
  * [Youtube plugin](plugins/youtube.md) 

--------------------------------------------------------------------------------
     
Related Information
-------------------

  * [Category manager](categories.md) 
  * [Inserting and editing category](category.md) 