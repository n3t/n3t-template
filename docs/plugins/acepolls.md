AcePolls plugin
===============

Description
-----------

This plugin allows to choose poll from AcePolls component. You can choose which
poll is displayed, and the way, it is inserted into the article.

This plugin was first presented in n3tTemplate 1.7.6.

--------------------------------------------------------------------------------

Usage
-----

Common usage of this plugin would be in combination with native AcePolls Plugin,
which allows to display polls inline in article.

--------------------------------------------------------------------------------

General Settings
----------------

#### Only published
If Yes selected, only published polls will be displayed. If No selected, all polls
will be displayed, regardless its state.

#### Ordering
Deteremines, how are displayed polls sorted.

 * __Oldest first__ - Oldest entries will be first
 * __Newest first__ - Newest entries will be first
 * __Alphabetically A-Z__ - Entries will be sorted aplhabetically A-Z
 * __Alphabetically Z-A__ - Entries will be sorted aplhabetically Z-A

#### Maximal count
Maximal items displayed. Empty value means no limit.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected poll is inserted into the article.
 
 * __Plugin__ - Code for native AcePolls Plugin will be inserted.
 * __Custom__ - Selected poll will be inserted into the article using the definition in the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following occurances will be replaced
 
 * __%ID%__ - ID of selected poll.
 * __%ALIAS%__ - alias of selected poll.
 * __%TITLE%__ - title of selected poll.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md) 