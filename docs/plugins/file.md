File plugin
===========

Description
-----------

File plugin brings possibility to choose file from your images directory or any
other directly from the server. You can specify the home directory for choosing
file, filter on displayed files, and define the way how selected file is inserted
to article.

This plugin was first presented in n3tTemplate 1.7.2.

--------------------------------------------------------------------------------

Usage
-----

This plugin could be used to create links to files stored on server (such as 
documents) or to select video file and insert [AllVideos Plugin][AllVideos] 
code to article.

--------------------------------------------------------------------------------

General Settings
----------------

#### Base path
This is root directory, where files starts to display. It defaults to images. 
Be carefull setting this path, as wrong valu can reveal files on your webserver 
to unauthorized people.

#### Filter
Comma separated file extensions to display. Empty valu means display all file types.

#### Maximal depth
How many levels of directory tree to display. Empty value means no limit. 0 (zero) 
means files only within the base path are displayed. Any other number means how 
many levels of subfolders could be displayed.

#### Maximal count
Maximal items displayed within one directory. This includes subfolders and/or files.
Empty value means no limit.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected file is inserted into the article.
 
 * __Link to File__ - Simple link to file will be inserted. Textof the link will 
   be the filename including extension.
 * __AllVideos Plugin__ - Code for [AllVideos Plugin][AllVideos] will be inserted.
 * __Custom__ - File will be inserted into the article using the definition in 
   the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following 
occurances will be replaced
 
 * __%BASENAME%__ - chosen filename with extension
 * __%FILENAME%__ - chosen filename without extension
 * __%EXTENSION%__ - chosen file extension
 * __%PATH%__ - path to chosen file relative to basepath
 * __%FILEPATH%__ - chosen filename including path relative to basepath
 * __%BASEPATH%__ - base path from the settings

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md)

[AllVideos]: http://extensions.joomla.org/extensions/extension/multimedia/multimedia-players/allvideos