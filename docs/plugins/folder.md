Folder plugin
=============

Description
-----------

Folder plugin brings possibility to choose folder from your images directory or
any other directly from the server. You can specify the home directory for choosing
folder and define the way how selected folder is inserted to article.

This plugin was first presented in n3tTemplate 1.7.2.

--------------------------------------------------------------------------------

Usage
-----

Main usage of this plugin should be in combination with some image gallery plugin.
It allows to easily choose folder, where images for gallery are stored.

--------------------------------------------------------------------------------

General Settings
----------------

#### Base path
This is root directory, where folders starts to display. It defaults to images. 
Be carefull setting this path, as wrong value can reveal folders on your webserver 
to unauthorized people.

#### Maximal depth
How many levels of directory tree to display. Empty value means no limit. 0 (zero) 
means folders only within the base path are displayed. Any other number means how 
many levels of subfolders could be displayed.

#### Show Empty
If No, empty folders will not be displayed in the tree. if Yes, any folder will be displayed.

#### Maximal count
Maximal items (subfolders) displayed within one directory. Empty value means no limit.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected folder is inserted into the article. 

 * __Simple Image Gallery Plugin__ - Code for [Simple Image Gallery Plugin][SIG] will be inserted.
 * __pPGallery Plugin__ - Code for [pPGallery Plugin][pPGallery] will be inserted.
 * __CSS Gallery Plugin__ - Code for [CSS Gallery Plugin][CSSGallery] will be inserted.
 * __Mp3 Browser Plugin__ - Code for [Mp3 Browser Plugin][MP3Browser] will be inserted.
 * __Custom__ - Folder will be inserted into the article using the definition in the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following 
occurances will be replaced 

 * __%PATH%__ - path to chosen folder relative to basepath
 * __%BASEPATH%__ - base path from the settings

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md)
 
[SIG]: http://extensions.joomla.org/extensions/extension/photos-a-images/galleries/simple-image-gallery
[pPGallery]: http://extensions.joomla.org/extensions/extension/photos-a-images/galleries/ppgallery
[CSSGallery]: http://extensions.joomla.org/extensions/extension/photos-a-images/galleries/css-gallery
[MP3Browser]: http://extensions.joomla.org/extensions/extension/multimedia/multimedia-players/mp3-browser