Module plugin
=============

Description
-----------

Module plugin brings possibility to choose installed Joomla! module. You can choose
if unpublished modules are shown, and the way selected module is inserted into the article.

This plugin was first presented in n3tTemplate 1.7.1.

--------------------------------------------------------------------------------

Usage
-----

Common usage of this plugin should be to make administrators life easier, when
using Joomla! native LoadModule plugin.

--------------------------------------------------------------------------------

General Settings
----------------

#### Only published
If Yes selected, only published modules will be displayed. If No selected, modules
will be displayed regardless its state.

#### Maximal count
Maximal items displayed. Empty value means no limit.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected file is inserted into the article.
 
 * __Loadmodule Plugin__ - Code for native Joomla! Loadmodule plugin will be inserted. 
   Note that this plugin exists in Joomla! 1.6 and newer only.
 * __Modules Anywhere Plugin__ - Code for popular [Modules Anywhere Plugin][ModulesAnywhere] 
   will be inserted.
 * __Custom__ - Selected module will be inserted into the article using the 
   definition in the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following occurances will be replaced
 
 * __%MODULE%__ - chosen module. This is for example "mod_custom"
 * __%ID%__ - chosen module ID. This is Joomla! internal ID (number) of the module.
 * __%TITLE%__ - chosen module title. This is the text, inserted in module as title.
 * __%NAME%__ - chosen module name. This is the value of module without prefix "mod_". 
   For example "custom".

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md)

[ModulesAnywhere]: http://extensions.joomla.org/extensions/extension/core-enhancements/coding-a-scripts-integration/modules-anywhere