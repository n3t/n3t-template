Phoca Maps plugin
=================

Description
-----------

This plugin allows to choose map from popular [Phoca Maps][PhocaMaps] component. 
You can choose which maps are displayed, and the way, they are inserted into the article.

This plugin was first presented in n3tTemplate 1.7.2.

--------------------------------------------------------------------------------

Usage
-----

Common usage of this plugin would be in combination with [Phoca Maps Plugin][PhocaMapsPlugin], 
which allows to display maps inline in article.

--------------------------------------------------------------------------------

General Settings
----------------

#### Only published
If Yes selected, only published maps will be displayed. If No selected, all maps 
will be displayed, regardless its state.

#### Ordering
Deteremines, how are displayed maps sorted. 

 * __Oldest first__ - Oldest entries will be first
 * __Newest first__ - Newest entries will be first
 * __Alphabetically A-Z__ - Entries will be sorted aplhabetically A-Z
 * __Alphabetically Z-A__ - Entries will be sorted aplhabetically Z-A

#### Maximal count
Maximal items displayed. Empty value means no limit.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected map is inserted into the article.
 
 * __Map__ - Code for [Phoca Maps Plugin][PhocaMapsPlugin] to display map directly 
   in the article will be inserted.
 * __Link__ - Code for [Phoca Maps Plugin][PhocaMapsPlugin] to link to map will 
   be inserted.
 * __Custom__ - Selected map will be inserted into the article using the definition 
   in the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following occurances will be replaced 

 * __%ID%__ - ID of selected map.
 * __%ALIAS%__ - alias of selected map.
 * __%TITLE%__ - title of selected map.
 * __%DESCRIPTION%__ - description of selected map.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md)

[PhocaMaps]: http://extensions.joomla.org/extensions/extension/maps-a-weather/maps-a-locations/phoca-maps
[PhocaMapsPlugin]: http://extensions.joomla.org/extensions/extension/maps-a-weather/maps-a-locations/phoca-maps-plugin