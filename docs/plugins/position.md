Position plugin
===============

Description
-----------

Position plugin brings possibility to choose Joomla! template position. You can 
choose if only positions containing published modules are shown, and the way 
selected position is inserted into the article.

This plugin was first presented in n3tTemplate 1.7.0.

--------------------------------------------------------------------------------

Usage
-----

Common usage of this plugin should be to make administrators life easier, when using
Joomla! native LoadPosition plugin.

--------------------------------------------------------------------------------

General Settings
----------------

#### Only published
If Yes selected, only positions containing published modules will be displayed.

If No selected, all positions will be displayed, regardless it contains any
published modules.

#### Maximal count
Maximal items displayed. Empty value means no limit.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected file is inserted into the article. 

 * __Loadposition Plugin__ - Code for native Joomla! Loadposition plugin will be 
   inserted.
 * __Modules Anywhere Plugin__ - Code for popular [Modules Anywhere Plugin][ModulesAnywhere] 
   will be inserted.
 * __Custom__ - Selected module will be inserted into the article using the definition
   in the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following 
occurances will be replaced 

 * __%POSITION%__ - chosen position. This is for example "user_1"

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md)

[ModulesAnywhere]: http://extensions.joomla.org/extensions/extension/core-enhancements/coding-a-scripts-integration/modules-anywhere  