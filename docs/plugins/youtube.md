Youtube plugin
==============

Description
-----------

This plugin allows to choose [youtube.com][youtube] video. In the settings you can
choose source of displayed videos (standart channel, user chanel, user favorites etc.)
and the way how it is inserted to article.

This plugin was first presented in n3tTemplate 1.7.0.

--------------------------------------------------------------------------------

Usage
-----

Most common usage would be displaying youtube video in the article in some way. 
Plugin can produce link to video, image preview of video or code for popular
[AllVideos plugin][AllVideos].

--------------------------------------------------------------------------------

General Settings
----------------

#### Source
Select the source of videos 

 * __Standard feed__ - One of the standard feeds (see the next settings) will be 
   used to display videos. This option is effected by other settings and can be 
   limited to Region and/or Category.
 * __User uploads__ - User (channel) uploaded videos (select user in the next settings)
 * __User favorites__ - User (channel) fovorites videos (select user in the next settings)
 * __Playlist__ - Playlist videos (select playlist in the next settings)

#### Standard feed
Select which standard feed to use, if standard feed is chosen in Source option
 
 * __Top rated__ - This feed contains the most highly rated YouTube videos.
 * __Top favorites__ - This feed contains videos most frequently flagged as favorite videos.
 * __Most viewed__ - This feed contains the most frequently watched YouTube videos.
 * __Most shared__ - This feed lists the YouTube videos most frequently shared on 
   Facebook and Twitter. This feed is available as an experimental feature.
 * __Most popular__ - This feed contains the most popular YouTube videos, selected 
   using an algorithm that combines many different signals to determine overall popularity.
 * __Most recent__ - This feed contains the videos most recently submitted to YouTube.
 * __Most discussed__ - This feed contains the YouTube videos that have received the most comments.
 * __Most responded__ - This feed contains YouTube videos that receive the most video responses.
 * __Recently featured__ - This feed contains videos recently featured on the 
   YouTube home page or featured videos tab.
 * __Trending videos__ - This feed lists trending videos as seen on YouTube Trends, 
   which surfaces popular videos as their popularity is increasing and also analyzes
   broader trends developing within the YouTube community. This feed is available as 
   an experimental feature.

#### Region
Allows to limit Standard feed to specific region.

#### Category
Allows to limit Standard feed to specific category.

#### User
Specify username (channel name) to use, when User uploads or User favorites is selected in Source option. Username could be identified for example from url of the channel homepage (www.youtube.com/user/username).

#### Playlist
Specify playlist ID to use, when Playlist is selected in Source option. This could be identified for example from URL address of playlist. It should be the data string behind the last slash.

#### Maximal count
Maximal videos displayed. Empty value means that the limit will be taken default youtube API limit (25 when writing this manual).

#### Format
Choose if you want to limit displayed videos to one format. If other value than All is selected, only videos supporting chosen format will be displayed.
 
Select which standard feed to use, if standard feed is chosen in Source option
 
 * __All__ - Display all videos regardless format it support.
 * __H.263__ - RTSP streaming URL for mobile video playback. H.263 video (up to 176x144) 
   and AMR audio.
 * __SWF__ - Embeddable player. This format is not available for a video that is not embeddable.
 * __MPEG-4 SP__ - RTSP streaming URL for mobile video playback. MPEG-4 SP video 
   (up to 176x144) and AAC audio.

--------------------------------------------------------------------------------

Output Settings
---------------

#### Output
This setting influence the way selected file is inserted into the article.
 
 * __Text link__ - Text link to video page will be inserted into the article. 
   As text will be used name of the video.
 * __Big preview__ - Image link, using the big preview image file will be inserted 
   into the article.
 * __Small preview__ - Image link, using the small preview image file will be 
   inserted into the article.
 * __Plugin - JW AllVideos__ - Code for [AllVideos plugin][AllVideos] will be inserted.
 * __Custom__ - Selected video will be inserted into the article using the definition 
   in the Custom code field.

#### Custom code
Specify the cutom code to insert into the article. Use any HTML code. Following 
occurances will be replaced 

 * __%ID%__ - ID of the selected video.
 * __%TITLE%__ - Title of the selecetd video.
 * __%DESCRIPTION%__ - Description of the selected video.
 * __%PLAYER%__ - SWF video player.
 * __%CONTENT_1%__ - Link to H.263 video format
 * __%CONTENT_5%__ - Link to SWF video player format.
 * __%CONTENT_6%__ - Link to MPEG-4 SP video format.
 * __%THUMBNAIL_HQDEFAULT%__ - Thumbnail of video in High quality.
 * __%THUMBNAIL_HQDEFAULT_WIDTH%__ - HQ thumbnail width.
 * __%THUMBNAIL_HQDEFAULT_HEIGHT%__ - HQ thumbnail height
 * __%THUMBNAIL_DEFAULT%__ - Default thumbnail of the video.
 * __%THUMBNAIL_DEFAULT_WIDTH%__ - Default thumbnail width.
 * __%THUMBNAIL_DEFAULT_HEIGHT%__ - Default thumbnail height.
 * __%THUMBNAIL_POSTER%__ - Thumbnail of the video in poster size.
 * __%THUMBNAIL_POSTER_WIDTH%__ - Poster thumbnail width.
 * __%THUMBNAIL_POSTER_HEIGHT%__ - Poster thumbnail height.
 * __%THUMBNAIL_START%__ - Thumbnail from the beginning of the video.
 * __%THUMBNAIL_START_WIDTH%__ - Start thumbnail width.
 * __%THUMBNAIL_START_HEIGHT%__ - Start thumbnail height.
 * __%THUMBNAIL_MIDDLE%__ - Thumbnail from the beginning of the video.
 * __%THUMBNAIL_MIDDLE_WIDTH%__ - Middle thumbnail width.
 * __%THUMBNAIL_MIDDLE_HEIGHT%__ - Middle thumbnail height.
 * __%THUMBNAIL_END%__ - Thumbnail from the end of the video.
 * __%THUMBNAIL_END_WIDTH%__ - End thumbnail width.
 * __%THUMBNAIL_END_HEIGHT%__ - End thumbanil height.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Plugins general informations](../plugins.md) 
  * [Categories](../categories.md) 
  * [Inserting and editing category](../category.md)

[youtube]: https://www.youtube.com/
[AllVideos]: http://extensions.joomla.org/extensions/extension/multimedia/multimedia-players/allvideos    