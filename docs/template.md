Inserting and editing template
==============================

How to Access
-------------

In the [template manager](templates.md) click on toolbar icon New, or select 
template in the list and click on its title to edit it.

--------------------------------------------------------------------------------

Description
-----------

Templates contains the definition of text, that will be used in the article. 
It can be defined as simple template, which appears in the [editor button](button.md)
tree, as automatically loading template in the editor for specified content categories,
or as automatically diaplyed text in specified content categories.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/template/screenshot.jpg)

--------------------------------------------------------------------------------

Fields
------

#### Title
Title of this template. It is not allowed to have duplicate titles within parent category.

#### Note
This note is for internall use, and will appear in the administration only.

#### Catgory
Specify parent category of this template. In the [editor button](button.md) tree 
this template will appear as nested within its parent category. If you want to
create template, which appears directly on the root, choose Uncategorized. 
Only simple categories (not plugin categories) can contain templates.

#### Published
Specify, if this template is pulished or not. Unpublished templates are not visible
in the [editor button](button.md) tree, not even are loaded as automatic in any context.

#### Access
Specify Access level of users, whose can see this template in the [editor button](button.md) tree.

#### Display access
Specify Access level of users, whose can see this template loaded as automatic in
any context. Using this you can specify the group of users to see this template
in the article, in the editor and so on.

#### Ordering
Specify ordering of this category within its parent category. When new category
is being inserted, this field is not enabled. New categories are implicitly ordered
at the end of its parent category.

#### Id
This is internal Id of this category. It cannot be changed.

#### Template
Here fill the code of the template itself. Do not use indirecttemplating (`{n3ttemplate}` code)
in the editor, if you plan to load ths template automtically in the content,
as n3tTemplate doesn't support recursive calls.

--------------------------------------------------------------------------------

Automatic templates
-------------------

In this section you can specify for which content categories is this template used
as Automatic and in which context. Note that only one template can be automatic for
each category in one context. This means, that you can have only on template for
one category automatically loading in editor, displaying at the top of article and so on.

#### Editor
Template will load automatically in the editor for here selected content categories.

#### Above article
Choose categories for which should be this template automatically displayed at the
beginning of full text of article. It means it will be displayed in such situations,
where the whole article is displayed.

#### Bellow article
Choose categories for which should be this template automatically displayed at the
end of full text of article. It means it will be displayed in such situations,
where the whole article is displayed.

#### Above article intro
Choose categories for which should be this template automatically displayed at the
beginning of intro text of article. It means it will be displayed in such situations,
where the intro text (the part before read more link) is displayed.

This is usually in the blog view, in the featured view or for example in module.

#### Bellow article intro
Choose categories for which should be this template automatically displayed at the
end of intro text of article. It means it will be displayed in such situations,
where the intro text (the part before read more link) is displayed. 

This is usually in the blog view, in the featured view or for example in module.

--------------------------------------------------------------------------------

Parameters
----------

Currently templates do not support any parameters.

--------------------------------------------------------------------------------

Toolbar
-------

![Toolbar](images/template/toolbar.jpg)

On the toolbar you can find following icons:

#### Save
Saves changes and stay in the edit mode.

#### Save & close
Saves changes and close the edit mode. Goes back to [Template manager](templates.md).

#### Save & New
Saves changes and open page to insert another template.

#### Save as Copy
Saves changes as new template.

#### Cancel
Closes the edit form without saving changes and goes back to [Template manager](templates.md).

#### Help
Will display help dialog.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Template manager](templates.md) 