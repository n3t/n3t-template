Template manager
================

How to Access
-------------

In the n3tTemplate [Control panel](cpanel.md) click on icon Templates, or whenever 
click on toolbar link Templates.

--------------------------------------------------------------------------------

Description
-----------

Templates manager allows you to create, delete and organize templates.

Templates can be organized in the tree of categories - every template can be placed 
into category, and in the [editor buttton](button.md) will appear as a child node 
of such category. More over, every template can be published or unpublished, have
specified Access level, Display access level, and of course has some title.

--------------------------------------------------------------------------------

Screenshot
----------

![Screenshot](images/templates/screenshot.jpg)

--------------------------------------------------------------------------------

Column headers
--------------

#### &#35;
This is just number of row in the list of templates. It has informational character.

#### Title
In this column you can see Title of the template, and if the template has filled 
optional field note, it will be visible here also.

#### Category
If template is placed within category, here will be displayed its Title.

#### Published
If category is published (visible to users) in this column will appear green checkmark. 
If unpublished, there will be red cross. Clicking on the icon will switch the state 
of category from published to unpublished or vice versa.

#### Access
In this column is vissible neccessary Access level for users to see this template 
in the [editor button](button.md) tree. For explanation of access levels please see the 
[Joomla! help][AccessLevels].

#### Display access
In this column is vissible neccessary Access level for users to load this template 
as [automatic](autotemplates.md). For explanation of access levels please see the 
[Joomla! help][AccessLevels].

#### Ordering
In this column is visible ordering of template within its parent category. 
If the list is sorted by this column, you can influence the ordering directly 
by clicking up / down icons, or fill the correct oredreing numbers in the boxes
for every displayed row, and click the save icon in the heading of this column.

Note that ordering s specified within the parent category, so not in all rows you
can see the up / down icons.

#### Id
This is internal ID of the template. This can be used to insert template to article
as link, using the code `{n3ttemplate id}`.

--------------------------------------------------------------------------------

List filters
------------

![Search](images/templates/filter-search.jpg)

On the left side above the list of templates you can specify filter. Enter the text,
you are searching for, and press the button Apply. Only templates containing eneterd
text in Title, Note or Template will be displayed. If you want to reset the filter
(display all templates), just press the button Reset.

![Filter](images/templates/filter.jpg)

On the right side above the list of templates, you can choose which templates to
display and / or specify category to display. If you select category fro the list,
only templates placed directly in this category will be shown. In the second filter,
if current is selected, all published and unpublished templates are displayed.

If you choose published or unpublished, only published / unpublished templates are
displayed. If you choose trashed, only trashed templates are displayed. 

Note that in trashed view, only some of columns are displayed.

--------------------------------------------------------------------------------

Toolbar
-------

On the toolbar you can find following icons:

![Toolbar](images/templates/toolbar.jpg)

#### Publish
Set chosen templates as published. Choose templates by checking the checkbox in 
the second column in the list.

#### Unpublish
Set chosen templates as unpublished. Choose templates by checking the checkbox in
the second column in the list.

#### New
Opens dialog for inserting new template.

#### Copy
Creates a copy of all selected templates. Those copies will have all the properties
and settings same as the original template, and will be placed in the same parent
category. Choose templates by checking the checkbox in the second column in the list.

#### Edit
Edits chosen template. Select the template by checking the checkbox in the second
column in the list.

#### Trash
Trashes chosen templates. Choose templates by checking the checkbox in the second
column in the list. Note that this action will note delete the template from database,
it just mark the template as trashed. Trashed templates can be displayed by chossing
trashed in the list filter.

#### Help
Will display help dialog.

If you filter list to display only trashed templates, toolbar will contain these icons:

![Toolbar trashed](images/templates/toolbar-trashed.jpg)

#### Restore
Restores selected templates from trash. Restored templates will be restored at the
end of the root category (will not have parent category, ordering set to 9999,
and will be unpublished). Choose templates by checking the checkbox in the second
column in the list.

#### Delete
Completely removes the selected templates from the database. Choose templates by
checking the checkbox in the second column in the list.

--------------------------------------------------------------------------------

Batch operations
----------------

Using batch operations you can easily change some parameters of multiple templates.

![Batch operations](images/templates/batch.jpg)

#### Select Category for Move/Copy
Select from the list category where you want selected templates move or copy, and
select if you want to Move or Copy.

#### Set Access Level
Select new Access level for all selected templates.

#### Set Display Access Level
Select new Display Access level for all selected templates.

To apply changes make your selections and click button Process.

--------------------------------------------------------------------------------

Tips
----

  * Click on the Title of a Template to edit it. 
  * Click on the green check mark or the red X in the Published column to toggle 
    between Published and Unpublished. 
  * Click on the Column Headers to sort Templates by that column. Click a second 
    time to sort descending (Z to A). Note that if you want to change templates 
    ordering, the list has to be ordered by ordering column.

--------------------------------------------------------------------------------

Related Information
-------------------

  * [Inserting and editing templates](template.md) 
  * [Automatic templates](autotemplates.md) 

[AccessLevels]: https://help.joomla.org/proxy/index.php?option=com_help&keyref=Help25:Users_Access_Levels    