<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

jimport( 'fof.utils.installscript.installscript' );

class com_n3ttemplateInstallerScript extends FOFUtilsInstallscript 
{
  protected $componentName = 'com_n3ttemplate';
  protected $componentTitle = 'n3t Template';
  
	protected $installation_queue = array(
		'plugins' => array(
			'editors-xtd' => array(
        'n3ttemplate' => true,                
			),
			'content' => array(                
        'n3ttemplate' => true,                
			),
			'n3ttemplate' => array(
        'file' => true,                                      
        'folder' => true,                                      
        'module' => true,                                      
        'phocamaps' => true,                                      
        'position' => true,                                      
        'youtube' => true,                                      
			),
		),
	);
   
	protected $postInstallationMessages = array(
		'demo' => array(
			'type' => 'action',
			'title_key' => 'COM_N3TTEMPLATE_POSTSETUP_DEMO_TITLE',
			'description_key' => 'COM_N3TTEMPLATE_POSTSETUP_DEMO_DESC',
			'action_key' => 'COM_N3TTEMPLATE_POSTSETUP_DEMO_BUTTON',
			'language_extension' => 'com_n3ttemplate',
			'language_client_id' => '1',
			'version_introduced' => '1.8.0',
			'condition_file' => 'admin://components/com_n3ttemplate/install/postinstall.php',
			'condition_method' => 'n3tTemplatePostInstallHasData',
			'action_file' => 'admin://components/com_n3ttemplate/install/postinstall.php',
			'action' => 'n3tTemplatePostInstallDemoData',
		),
	);  
  
	protected $uninstallation_queue = array(		
		'modules' => array(
			'admin' => array(
        'n3ttemplate_credits',
      )			
		),		
		'plugins' => array(
			'n3ttemplate' => array(
        'acepolls',                                      
        'djimageslider',                                      
			),
		),
	);  
  
	protected $removeFilesAllVersions = array(
		'files'   => array(
      'administrator/components/com_n3ttemplate/controllers/config.php',
      'administrator/components/com_n3ttemplate/controllers/cpanel.php',
      'administrator/components/com_n3ttemplate/controllers/info.php',
      'administrator/components/com_n3ttemplate/controllers/update.php',
      'administrator/components/com_n3ttemplate/helpers/acl.php',
      'administrator/components/com_n3ttemplate/helpers/content.php',
      'administrator/components/com_n3ttemplate/helpers/update.php',
      'administrator/components/com_n3ttemplate/install/demo.mysql.sql',
      'administrator/components/com_n3ttemplate/install/install.helper.php',
      'administrator/components/com_n3ttemplate/models/config.php',
      'administrator/components/com_n3ttemplate/models/cpanel.php',      
      'administrator/components/com_n3ttemplate/models/update.php',
      'administrator/components/com_n3ttemplate/view.php',
      'media/com_n3ttemplate/n3ttemplate.css',      
      'media/com_n3ttemplate/images/icon-16-n3ttemplate.png',
      'media/com_n3ttemplate/images/icon-32-save2copy.png',
      'media/com_n3ttemplate/images/icon-32-save2new.png',
      'media/com_n3ttemplate/images/icon-48-autotemplates.png',
      'media/com_n3ttemplate/images/icon-48-categories.png',
      'media/com_n3ttemplate/images/icon-48-n3ttemplate.png',
      'media/com_n3ttemplate/images/icon-48-new-category.png',
      'media/com_n3ttemplate/images/icon-48-new-template.png',
      'media/com_n3ttemplate/images/icon-48-plugins.png',
      'media/com_n3ttemplate/images/icon-48-templates.png',
      'media/com_n3ttemplate/images/icon-48-update.png',      
		),
		'folders' => array(
			'administrator/components/com_n3ttemplate/install/notes',
      'administrator/components/com_n3ttemplate/views/config',
      'administrator/components/com_n3ttemplate/views/cpanel',
      'administrator/components/com_n3ttemplate/views/info',
      'administrator/components/com_n3ttemplate/views/update',      
		)
	);  
}
