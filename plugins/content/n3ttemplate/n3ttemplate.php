<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die; 

class plgContentN3tTemplate extends JPlugin
{

  public function onContentPrepare($context, &$article, &$params, $page = 0) 	
	{ 
    switch ($context) {
      case 'com_content.article':
        $this->proccessAuto($article, true);
        break;  
      case 'com_content.category':
      case 'com_content.featured':
        $this->proccessAuto($article, false);
        break;  
    } 
    $this->proccess($article);
	}
	
	private function proccess(&$article) 
  {      
    if (strpos($article->text, '{n3ttemplate') === false) return true;       
    $regex		= '/{n3ttemplate\s+(.*?)}/i';
    preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);
		if ($matches){
			foreach ($matches as $match) {
				$id = trim($match[1]);
				$output = $this->loadTemplate($id);
				$article->text = preg_replace("|$match[0]|", addcslashes($output, '\\'), $article->text, 1);
			}
		}
  }           		
	
	private function proccessAuto(&$article, $fulltext) 
  {        	    
    $user = JFactory::getUser();
    $db = JFactory::getDBO();
    
		$query = $db->getQuery(true)
      ->select($db->qn(array('t.id','at.position')))
      ->from($db->qn('#__n3ttemplate_autotemplates').' AS at')
      ->from($db->qn('#__n3ttemplate_templates').' AS t')
      ->where($db->qn('t.id').'='.$db->qn('at.template_id'))
      ->where($db->qn('at.category_id').'='.$article->catid)
      ->where($db->qn('t.published').'=1')
      ->where($db->qn('display_access').' in ('.implode(',', $user->getAuthorisedViewLevels()).') ');
        
		$db->setQuery($query);
    $autotemplates = $db->loadObjectList();
        
    if ($autotemplates) {
      foreach($autotemplates as $autotemplate) {
        switch ($autotemplate->position) {
          case "top":
            if ($fulltext) 
              $article->text = $this->loadTemplate($autotemplate->id).$article->text;
            break;
          case "bottom":
            if ($fulltext) 
              $article->text.= $this->loadTemplate($autotemplate->id);
            break;
          case "introtop":
            if (!$fulltext) 
              $article->text = $this->loadTemplate($autotemplate->id).$article->text;
            break;
          case "introbottom":
            if (!$fulltext) 
              $article->text.= $this->loadTemplate($autotemplate->id);
            break;
        }
      }
    }
	}
			
	private function loadTemplate($id)
	{
	  $id = (int)$id;
	  if ($id) {
      $user = JFactory::getUser();
	    $db = JFactory::getDBO();
      
      $query = $db->getQuery(true)
        ->select($db->qn('template'))
        ->from($db->qn('#__n3ttemplate_templates'))
        ->where($db->qn('id').'='.$id)
        ->where($db->qn('published').'=1')
        ->where($db->qn('display_access').' in ('.implode(',', $user->getAuthorisedViewLevels()).') ');
  		$db->setQuery($query);
      return $db->loadResult();        		    
	  }
	  return "";
  }
  	
}