<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class plgButtonN3tTemplate extends JPlugin
{

  protected $autoloadLanguage = true;

	function onDisplay($name)
	{
		JHTML::_('behavior.modal');
    $link = 'index.php?option=com_n3ttemplate&amp;view=button&amp;tmpl=component&amp;e_name='.$name;
    
		$button = new JObject();
		$button->modal = true;
		$button->link = $link;
    $button->class = 'btn';
		$button->text = JText::_('PLG_EDITORS_XTD_N3TTEMPLATE_TEMPLATES');
		$button->name = 'file-2';
		$button->options = "{handler: 'iframe', size: {x: 570, y: 400}}";    

    $this->autoTemplate($name);
    
		return $button;
	}
	
	function autoTemplate($name) 
	{
	  $app = JFactory::getApplication();
	  if ($app->isAdmin()) {
      $useAutoTemplate = JRequest::getCmd('option')=='com_content'
        && JRequest::getCmd('view')=='article'
        && JRequest::getCmd('layout')=='edit'
        && !JRequest::getInt('id');
    } else {
      $useAutoTemplate = JRequest::getCmd('option')=='com_content'
        && JRequest::getCmd('view')=='form'
        && JRequest::getCmd('layout')=='edit'
        && !JRequest::getInt('a_id');
    }
    if ( $useAutoTemplate ) {            
      JHtml::_('jquery.framework');
      JHtml::script('com_n3ttemplate/jquery.n3ttemplate.min.js',false,true);

      JText::script('PLG_EDITORS_XTD_N3TTEMPLATE_AUTOTEMPLATE_CONFIRM_TITLE');  
      JText::script('PLG_EDITORS_XTD_N3TTEMPLATE_AUTOTEMPLATE_CONFIRM_REPLACE');  
      JText::script('JYES');  
      JText::script('JNO');  

      $doc = JFactory::getDocument();
      $getContent = $this->_subject->getContent($name); 
      $setContent = $this->_subject->setContent($name,'response');
      // JCE strange behavior
      $setContent = str_replace("'response'", "response", $setContent);

      $script =
        ';'."\n".
        'n3tTemplate = window.n3tTemplate || {};'."\n".
        'n3tTemplate.baseURI = "'.JURI::base().'";'."\n".
        'n3tTemplate.getContent = function() {return '.$getContent.'};'."\n".
        'n3tTemplate.setContent = function(response) {'.$setContent.'};'."\n";
      $doc->addScriptDeclaration($script); 
    }   	
	}
}
