<?php
/**
 * @package n3tTemplate
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

JRequest::setVar('view', 'button'); 
$lang = JFactory::getLanguage();
$lang->load('com_n3ttemplate',JPATH_ADMINISTRATOR);

require_once(JPATH_COMPONENT_ADMINISTRATOR.'/controller.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.'/model.php');

require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/button.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/html.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/plugin.php');

$controller=JRequest::getCmd('view','button');
$path = JPATH_COMPONENT_ADMINISTRATOR.'/controllers/'.$controller.'.php';
if(JFile::exists($path))
{
	require_once($path);
}
else 
{
	JError::raiseError('500',JText::_('Unknown controller').' '.$controller);
}

$controllerName='n3tTemplateController'.ucfirst($controller);

$controller=new $controllerName();
$controller->addViewPath(JPATH_COMPONENT_ADMINISTRATOR.'/views');
$controller->addModelPath(JPATH_COMPONENT_ADMINISTRATOR.'/models');

$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
